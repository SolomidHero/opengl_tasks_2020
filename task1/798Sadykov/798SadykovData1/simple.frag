#version 330

in float height;
out vec4 fragColor;

void main() {
	// interpolation by color
  // fragColor = vec4(0.5, 0.0, 1.0, 1.0);
  fragColor = mix(vec4(0.0, 1.0, 1.0, 1), vec4(0.25, 0.25, 0.0, 1), 1. / (1. + exp(5 * height)));
}