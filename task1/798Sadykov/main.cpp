#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/rotate_vector.hpp>

#include <iostream>
#include <vector>

#include "terrain.hpp"
#include "loaders.h"


// window
constexpr float windowWidth = 800;
constexpr float windowHeight = 600;
// timing
float deltaTime = 0.0f;
float lastFrame = 0.0f;

// Starting camera settings
glm::vec3 cameraPos   = glm::vec3(0.0f, 1.5f, 0.0f);
glm::vec3 cameraArrow = glm::normalize(glm::vec3(0.01f, -1.0f, 0.0f));
glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f, 0.0f);
constexpr float cameraRelativeSpeed = 0.8f;

bool firstMouse = true;
float yaw   = 0.0f;	// yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right so we initially rotate a bit to the left.
float pitch = -90.0f;
float lastX =  800.0 / 2.0;
float lastY =  600.0 / 2.0;
float fov   =  35.0f;
constexpr float sensitivity = 0.07f;

// Starting values for view, model, projection matrices
glm::mat4 viewMatrix = glm::lookAt(
    cameraPos,               // eye
    cameraPos + cameraArrow, // center
    cameraUp                 // up 
);
glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, 1.0f, -0.5f));
glm::mat4 projectionMatrix = glm::mat4(1.0f);

//Функция обратного вызова для обработки событий клавиатуры
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ENTER) {
        //Если нажата клавиша ENTER, то закрываем окно
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
    float cameraSpeed = cameraRelativeSpeed * deltaTime;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        cameraPos += cameraSpeed * cameraArrow;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        cameraPos -= cameraSpeed * cameraArrow;
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        cameraPos -= glm::normalize(glm::cross(cameraArrow, cameraUp)) * cameraSpeed;
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        cameraPos += glm::normalize(glm::cross(cameraArrow, cameraUp)) * cameraSpeed;
    }
}

// whenever the mouse moves, this callback is called
void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
    if (firstMouse) {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top
    lastX = xpos;
    lastY = ypos;

    xoffset *= sensitivity;
    yoffset *= sensitivity;

    yaw += xoffset;
    pitch += yoffset;

    // make sure that when pitch is out of bounds, screen doesn't get flipped
    pitch = glm::max(-89.0f, glm::min(89.0f, pitch));

    glm::vec3 arrow;
    // arrow = glm::rotate(arrow, glm::radians(pitch), glm::cross(glm::vec3(0.1f, 0.1f, -1.0f), glm::vec3(0.5f, 0.5f, 0.0f)));
    // arrow = glm::rotate(arrow, glm::radians(yaw), glm::cross(
    //     glm::cross(glm::vec3(0.5f, 0.5f, 0.0f), glm::vec3(0.1f, 0.1f, -1.0f)), glm::vec3(0.1f, 0.1f, -1.0f)
    // ));

    arrow.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    arrow.y = sin(glm::radians(pitch));
    arrow.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    cameraArrow = glm::normalize(arrow);
}

int main() {
    //Инициализируем библиотеку GLFW
    if (!glfwInit()) {
        std::cerr << "ERROR: could not start GLFW3\n";
        exit(1);
    }

    //Устанавливаем параметры создания графического контекста
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    //Создаем графический контекст (окно)
    GLFWwindow* window = glfwCreateWindow(windowWidth, windowHeight, "MIPT OpenGL project", nullptr, nullptr);
    if (!window) {
        std::cerr << "ERROR: could not open window with GLFW3\n";
        glfwTerminate();
        exit(1);
    }

    //Делаем этот контекст текущим
    glfwMakeContextCurrent(window);

    //Устанавливаем функцию обратного вызова для обработки событий клавиатуры
    glfwSetKeyCallback(window, keyCallback);
    glfwSetCursorPosCallback(window, mouseCallback);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    //Инициализируем библиотеку GLEW
    glewExperimental = GL_TRUE;
    glewInit();

    //=========================================================

    // Terrain
    Terrain terrain(50, 50);
    auto triangles = terrain.get_triangles();
    auto normals = terrain.get_normals(); // example, result not used

    //Создаем буфер VertexBufferObject для хранения координат на видеокарте
    GLuint vbo;
    glGenBuffers(1, &vbo);

    //Делаем этот буфер текущим
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    //Копируем содержимое массива в буфер на видеокарте
    glBufferData(GL_ARRAY_BUFFER, 9 * triangles.size() * sizeof(float), triangles.data(), GL_STATIC_DRAW);

    //=========================================================

    //Создаем объект VertexArrayObject для хранения настроек полигональной модели
    GLuint vao;
    glGenVertexArrays(1, &vao);

    //Делаем этот объект текущим
    glBindVertexArray(vao);

    //Делаем буфер с координатами текущим
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    //Включаем 0й вершинный атрибут
    glEnableVertexAttribArray(0); // for vertices
    glEnableVertexAttribArray(1); // for normals

    glEnable(GL_DEPTH_TEST);

    // Устанавливаем настройки:
    //0й атрибут,
    //3 компоненты типа GL_FLOAT,
    //не нужно нормализовать,
    //0 - значения расположены в массиве впритык,
    //0 - сдвиг от начала
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(0)); // for vertices
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(0)); // for normals

    glBindVertexArray(0);

    //=========================================================

    //Вершинный шейдер
    std::string vertexShaderText = loadshader("798SadykovData1/simple.vert");
    const GLchar* vertexShaderTextPtr = static_cast<const GLchar*>(vertexShaderText.c_str());

    //Создаем шейдерный объект
    GLuint vs = glCreateShader(GL_VERTEX_SHADER);

    //Передаем в шейдерный объект текст шейдера
    glShaderSource(vs, 1, &vertexShaderTextPtr, nullptr);

    //Компилируем шейдер
    glCompileShader(vs);

    //Проверяем ошибки компиляции
    int status = -1;
    glGetShaderiv(vs, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE) {
        GLint errorLength;
        glGetShaderiv(vs, GL_INFO_LOG_LENGTH, &errorLength);

        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);

        glGetShaderInfoLog(vs, errorLength, 0, errorMessage.data());

        std::cerr << "Failed to compile vertex shader:\n" << errorMessage.data() << std::endl;

        exit(1);
    }

    //=========================================================

    //Фрагментный шейдер
    std::string fragmentShaderText = loadshader("798SadykovData1/simple.frag");
    const GLchar* fragmentShaderTextPtr = static_cast<const GLchar*>(fragmentShaderText.c_str());

    //Создаем шейдерный объект
    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);

    //Передаем в шейдерный объект текст шейдера
    glShaderSource(fs, 1, &fragmentShaderTextPtr, nullptr);

    //Компилируем шейдер
    glCompileShader(fs);

    //Проверяем ошибки компиляции
    status = -1;
    glGetShaderiv(fs, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE) {
        GLint errorLength;
        glGetShaderiv(fs, GL_INFO_LOG_LENGTH, &errorLength);

        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);

        glGetShaderInfoLog(fs, errorLength, 0, errorMessage.data());

        std::cerr << "Failed to compile fragment shader:\n" << errorMessage.data() << std::endl;

        exit(1);
    }

    //=========================================================

    //Создаем шейдерную программу
    GLuint program = glCreateProgram();

    //Прикрепляем шейдерные объекты
    glAttachShader(program, vs);
    glAttachShader(program, fs);

    //Линкуем программу
    glLinkProgram(program);

    //Проверяем ошибки линковки
    status = -1;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status != GL_TRUE) {
        GLint errorLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &errorLength);

        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);

        glGetProgramInfoLog(program, errorLength, 0, errorMessage.data());

        std::cerr << "Failed to link the program:\n" << errorMessage.data() << std::endl;

        exit(1);
    }

    //=========================================================

    //Добавляем uniform переменные
    glUseProgram(program);
    GLint view = glGetUniformLocation(program, "viewMatrix");
    GLint model = glGetUniformLocation(program, "modelMatrix");
    GLint projection = glGetUniformLocation(program, "projectionMatrix");

    //Цикл рендеринга (пока окно не закрыто)
    while (!glfwWindowShouldClose(window)) {
        // Используем время между генерациями фреймов
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        //Проверяем события ввода (здесь вызывается функция обратного вызова для обработки событий клавиатуры)
        glfwPollEvents();
        viewMatrix = glm::lookAt(cameraPos, cameraPos + cameraArrow, cameraUp);
        projectionMatrix = glm::perspective(glm::radians(fov), (float)windowWidth / (float)windowHeight, 0.01f, 10.0f);


        //Отправляем значения в шейдерную программу
        glUniformMatrix4fv(view, 1, GL_FALSE, glm::value_ptr(viewMatrix));
        glUniformMatrix4fv(model, 1, GL_FALSE, glm::value_ptr(modelMatrix));
        glUniformMatrix4fv(projection, 1, GL_FALSE, glm::value_ptr(projectionMatrix));


        //Получаем размеры экрана (окна)
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);

        //Устанавливаем порт вывода на весь экран (окно)
        glViewport(0, 0, width, height);

        //Очищаем порт вывода (буфер цвета и буфер глубины)
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдерную программу
        glUseProgram(program);

        //Подключаем VertexArrayObject с настойками полигональной модели
        glBindVertexArray(vao);

        //Рисуем полигональную модель (состоит из треугольников, сдвиг 0, количество вершин 3)
        glDrawArrays(GL_TRIANGLES, 0, 3 * triangles.size());

        glfwSwapBuffers(window); //Переключаем передний и задний буферы
    }

    //Удаляем созданные объекты OpenGL
    glDeleteProgram(program);
    glDeleteShader(vs);
    glDeleteShader(fs);
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);

    glfwTerminate();

    return 0;
}