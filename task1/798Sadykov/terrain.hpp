#include "noise.hpp"
#include <glm/vec3.hpp>
#include <glm/gtx/normal.hpp>

#include <vector>
#include <array>
#include <iostream>


class Terrain {
 public:
  Terrain(size_t width, size_t height)
    : width(width), height(height),
      verts(width * height), faces(2 * (width - 1) * (height - 1)),
      noiser(PerlinNoise(1, 0.2, 0.2, 7, 4)) { generate(); }

  void generate();
  std::vector<std::array<glm::vec3, 3>> get_triangles();
  std::vector<glm::vec3> get_normals();

 private:
  size_t pos(size_t i, size_t j, bool is_pt);

 private:
  size_t width;
  size_t height;

  // Board vertices and faces
  std::vector<glm::vec3> verts;
  std::vector<std::array<size_t, 3>> faces;

  // Noise class
  PerlinNoise noiser;
};

size_t Terrain::pos(size_t i, size_t j, bool is_pt=true) {
  return is_pt? i * width + j : 2 * (i * (width - 1) + j);
}

void Terrain::generate() {
  // Create vertices
  float spacing = 1.0;

  // Rows - i-th coordinate (same as z)
  // Cols - j-th coordinate (same as x)
  // [i, j] = i * width + j
  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
      // NOTE: origin is not at center of mesh
      float y = noiser.GetHeight(
        static_cast<float>(j) / width,
        static_cast<float>(i) / height
      );

      // std::cout << (int)y << "\t ";
      verts[pos(i, j)] = {j * spacing / width, y * spacing, i * spacing / height};
    }
    // std::cout << std::endl;
  }

  // Generate faces
  // Rows (-1 for last)
  for (int i = 0; i < height - 1; i++) {
    // Cols (-1 for last)
    for (int j = 0; j < width - 1; j++) {
      // Every 4 vertices that make square we divide by one diagonal,
      // hence two faces: upper and lower triangle

      // lower
      faces[pos(i, j, false)] = {
        pos(i, j),
        pos(i, j + 1),
        pos(i + 1, j),
      };

      // upper
      faces[pos(i, j, false) + 1] = {
        pos(i + 1, j),
        pos(i + 1, j + 1),
        pos(i, j + 1),
      };
    }
  }
}


std::vector<std::array<glm::vec3, 3>> Terrain::get_triangles() {
  std::vector<std::array<glm::vec3, 3>> triangles(faces.size());
  for (size_t i = 0; i < faces.size(); ++i) {
    triangles[i] = { verts[faces[i][0]], verts[faces[i][1]], verts[faces[i][2]] };
  }

  return triangles;
}

std::vector<glm::vec3> Terrain::get_normals() {
  std::vector<glm::vec3> normales(verts.size());
  for (size_t i = 0; i < height; ++i) {
    for (size_t j = 0; j < width; ++j) {
      glm::vec3 normal(0.0f);
      auto& cur_vert = verts[pos(i, j)];

      if (i < height - 1 && j < width - 1) normal += glm::triangleNormal(cur_vert, verts[pos(i + 1, j)], verts[pos(i, j + 1)]);
      if (i < height - 1 && j > 0) normal += glm::triangleNormal(cur_vert, verts[pos(i + 1, j)], verts[pos(i, j - 1)]);
      if (i > 0 && j < width - 1) normal += glm::triangleNormal(cur_vert, verts[pos(i - 1, j)], verts[pos(i, j + 1)]);
      if (i > 0 && j > 0) normal += glm::triangleNormal(cur_vert, verts[pos(i - 1, j)], verts[pos(i, j - 1)]);

      normales[pos(i, j)] = glm::normalize(normal);
    }
  }

  return normales;
}