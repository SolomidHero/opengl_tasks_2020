#include <fstream>
#include <sstream>

std::string loadshader(const std::string& filename) {
  std::ifstream file(filename);
  std::cout << filename << std::endl;
  std::stringstream buffer;
  buffer << file.rdbuf() << "\0";

  std::cout << buffer.str() << std::endl;
  return buffer.str();
}
