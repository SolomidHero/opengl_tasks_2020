#include <GL/glew.h>
#include <glm/vec3.hpp>
#include <glm/gtx/normal.hpp>

#include <vector>
#include <array>


class Box {
 public:
  Box(glm::vec3 center, glm::vec3 up, glm::vec3 right, float radius);

  std::vector<std::array<glm::vec3, 3>> get_triangles();
  std::vector<glm::vec3> get_triangles_normals();

 private:
  size_t to_index(int x, int y, int z);
  std::array<int, 3> to_pos(size_t index);

 private:
  std::array<glm::vec3, 8> vertices;
  std::array<std::array<size_t, 3>, 12> faces;
};

Box::Box(glm::vec3 center, glm::vec3 up, glm::vec3 right, float radius=1.0) {
  up = glm::normalize(up);
  right = glm::normalize(right);
  auto front = glm::normalize(glm::cross(up, right));


  for (size_t i = 0; i < vertices.size(); ++i) {
    auto [x, y, z] = to_pos(i);

    vertices[i] = radius * (
      center +
      static_cast<float>(x) * right +
      static_cast<float>(y) * up +
      static_cast<float>(z) * front
    );
  }

  faces = {{
    {to_index(1, -1, -1), to_index(1, 1, -1), to_index(1, -1, 1)},    // right1
    {to_index(1, -1, 1), to_index(1, 1, -1), to_index(1, 1, 1)},      // right2
    {to_index(-1, -1, -1), to_index(-1, -1, 1), to_index(-1, 1, -1)}, // left1
    {to_index(-1, -1, 1), to_index(-1, 1, 1), to_index(-1, 1, -1)},   // left2
    {to_index(-1, -1, 1), to_index(1, -1, 1), to_index(-1, 1, 1)},    // front1
    {to_index(1, -1, 1), to_index(1, 1, 1), to_index(-1, 1, 1)},      // front2
    {to_index(-1, -1, -1), to_index(-1, 1, -1), to_index(1, -1, -1)}, // back1
    {to_index(1, -1, -1), to_index(-1, 1, -1), to_index(1, 1, -1)},   // back2
    {to_index(-1, 1, -1), to_index(-1, 1, 1), to_index(1, 1, -1)},    // top1
    {to_index(1, 1, -1), to_index(-1, 1, 1), to_index(1, 1, 1)},      // top2
    {to_index(-1, -1, -1), to_index(1, -1, -1), to_index(-1, -1, 1)}, // bottom1
    {to_index(-1, -1, 1), to_index(1, -1, -1), to_index(1, -1, 1)},   // bottom2
  }};
}

size_t Box::to_index(int x, int y, int z) {
  assert(x == 1 || x == -1);
  assert(y == 1 || y == -1);
  assert(z == 1 || z == -1);

  // in [0, 7]
  return (x + 2 * y + 4 * z + 7) / 2;
}

std::array<int, 3> Box::to_pos(size_t index) {
  return {
    (static_cast<int>(index) & 1)? 1 : -1,
    (static_cast<int>(index) & 2)? 1 : -1,
    (static_cast<int>(index) & 4)? 1 : -1
  };
}


std::vector<std::array<glm::vec3, 3>> Box::get_triangles() {
  std::vector<std::array<glm::vec3, 3>> triangles(faces.size());
  for (size_t i = 0; i < faces.size(); ++i) {
    auto [v1, v2, v3] = faces[i];
    triangles[i] = { vertices[v1], vertices[v2], vertices[v3] };
  }

  return triangles;
}

std::vector<glm::vec3> Box::get_triangles_normals() {
  std::vector<glm::vec3> triangles_normals(faces.size());
  for (size_t i = 0; i < faces.size(); ++i) {
    auto [v1, v2, v3] = faces[i];
    triangles_normals[i] = glm::triangleNormal(vertices[v1], vertices[v2], vertices[v3]);
  }

  return triangles_normals;
}
