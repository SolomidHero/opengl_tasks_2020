#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/rotate_vector.hpp>

#include <iostream>
#include <vector>

#include "terrain.hpp"
#include "box.hpp"
#include "plain.hpp"
#include "loaders.h"
#include "shader.hpp"


// window
constexpr uint terrain_size = 100;
constexpr float windowWidth = 1200;
constexpr float windowHeight = 900;

// timing
float deltaTime = 0.0f;
float lastFrame = 0.0f;

// light
glm::vec3 lightArrow = glm::normalize(glm::vec3(0.0f, 1.5f, 0.0f));
glm::vec3 lightColor = glm::vec3(1.0f, 1.0f, 1.0f);
constexpr bool hdr = true;
constexpr float exposure = 1.5;

// Starting camera settings
glm::vec3 cameraPos   = glm::vec3(0.0f, 1.0f, 0.0f);
glm::vec3 cameraArrow = glm::normalize(glm::vec3(0.01f, -1.0f, 0.0f));
glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f, 0.0f);
constexpr float cameraRelativeSpeed = 0.5f;

bool firstMouse = true;
float yaw   = 0.0f;	// yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right so we initially rotate a bit to the left.
float pitch = -90.0f;
float lastX =  windowWidth / 2.0;
float lastY =  windowHeight / 2.0;
float fov   =  50.0f;
constexpr float sensitivity = 0.07f;

// water
const float waterLevel = -0.5f;
const float bottomLimit = -100.0f;
void update_camera_reflection();
glm::vec3 cameraReflectionPos;
glm::vec3 cameraReflectionArrow = cameraUp;
glm::vec3 cameraReflectionUp    = glm::vec3(0.0, 0.0, 1.0);
glm::mat4 projectionReflectionMatrix = glm::mat4(1.0f) * glm::scale(glm::vec3(1., -1., 1.));
glm::mat4 viewReflectionMatrix;
glm::mat4 waterModelMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, 1.0f, -0.5f));
float waterSurfaceSize = 5.;
float waterWindowWidth = waterSurfaceSize * 1000;
float waterWindowHeight = waterSurfaceSize * 1000;


// Starting values for view, model, projection matrices
glm::mat4 viewMatrix = glm::lookAt(
    cameraPos,               // eye
    cameraPos + cameraArrow, // center
    cameraUp                 // up 
);
glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, 1.0f, -0.5f));
glm::mat4 projectionMatrix = glm::mat4(1.0f);

//Функция обратного вызова для обработки событий клавиатуры
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ENTER) {
        //Если нажата клавиша ENTER, то закрываем окно
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
    float cameraSpeed = cameraRelativeSpeed * deltaTime;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        cameraPos += cameraSpeed * cameraArrow;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        cameraPos -= cameraSpeed * cameraArrow;
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        cameraPos -= glm::normalize(glm::cross(cameraArrow, cameraUp)) * cameraSpeed;
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        cameraPos += glm::normalize(glm::cross(cameraArrow, cameraUp)) * cameraSpeed;
    }
}

// whenever the mouse moves, this callback is called
void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
    if (firstMouse) {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top
    lastX = xpos;
    lastY = ypos;

    xoffset *= sensitivity;
    yoffset *= sensitivity;

    yaw += xoffset;
    pitch += yoffset;

    // make sure that when pitch is out of bounds, screen doesn't get flipped
    pitch = glm::max(-89.0f, glm::min(89.0f, pitch));

    glm::vec3 arrow;
    // arrow = glm::rotate(arrow, glm::radians(pitch), glm::cross(glm::vec3(0.1f, 0.1f, -1.0f), glm::vec3(0.5f, 0.5f, 0.0f)));
    // arrow = glm::rotate(arrow, glm::radians(yaw), glm::cross(
    //     glm::cross(glm::vec3(0.5f, 0.5f, 0.0f), glm::vec3(0.1f, 0.1f, -1.0f)), glm::vec3(0.1f, 0.1f, -1.0f)
    // ));

    arrow.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    arrow.y = sin(glm::radians(pitch));
    arrow.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    cameraArrow = glm::normalize(arrow);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    // make sure the viewport matches the new window dimensions; note that width, 
    // height will be significantly larger than specified on retina displays.
    std::cout << width << ":" << height << " vs " << windowWidth << ":" << windowHeight << std::endl;
    glViewport(0, 0, width, height);
}

void update_camera_reflection() {
    // float modelWaterLevel = (modelMatrix * glm::vec4(0.0, waterLevel, 0.0, 1.0)).y;
    float modelWaterLevel = waterLevel + 1.0;
    cameraReflectionPos   = glm::vec3(cameraPos.x, 2 * modelWaterLevel - cameraPos.y, cameraPos.z);
    waterModelMatrix = glm::translate(modelMatrix, glm::vec3(cameraPos.x, 0.0, cameraPos.z));
    // cameraReflectionArrow = glm::reflect(cameraArrow, cameraUp);

    viewReflectionMatrix  = glm::lookAt(
        cameraReflectionPos,
        cameraReflectionPos + cameraReflectionArrow,
        cameraReflectionUp
    );
    float reflection_fov = 2 * glm::atan(waterSurfaceSize, cameraPos.y - modelWaterLevel);
    projectionReflectionMatrix = glm::perspective(reflection_fov, 1.0f, 0.01f, 100.0f) * glm::scale(glm::vec3(1., -1., 1.));
}

int main() {
    //Инициализируем библиотеку GLFW
    if (!glfwInit()) {
        std::cerr << "ERROR: could not start GLFW3\n";
        exit(1);
    }


    //Устанавливаем параметры создания графического контекста
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    //Создаем графический контекст (окно)
    GLFWwindow* window = glfwCreateWindow(windowWidth, windowHeight, "MIPT OpenGL project", nullptr, nullptr);
    if (!window) {
        std::cerr << "ERROR: could not open window with GLFW3\n";
        glfwTerminate();
        exit(1);
    }

    //Делаем этот контекст текущим
    glfwMakeContextCurrent(window);

    //Устанавливаем функцию обратного вызова для обработки событий клавиатуры
    glfwSetKeyCallback(window, keyCallback);
    glfwSetCursorPosCallback(window, mouseCallback);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    //Инициализируем библиотеку GLEW
    glewExperimental = GL_TRUE;
    glewInit();

    // Включаем тесты глубины и смешивание
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //=========================================================

    // Terrain
    Terrain terrain(terrain_size, terrain_size);
    auto triangles = terrain.get_triangles();
    auto triangle_normals = terrain.get_triangles_normals();
    auto triangle_plain_coords = terrain.get_triangles_plain_coords();

    float mean_h = 0., min_h = +100., max_h = -100.;
    for (auto trg : triangles) {
        mean_h += trg[0].y;
        max_h = std::max(trg[0].y, max_h);
        min_h = std::min(trg[0].y, min_h);
    }
    mean_h /= triangles.size();
    std::cout << "[" << min_h << " - " << mean_h << " - " << max_h << "]" << std::endl;

    // Skybox
    Box skybox(
        glm::vec3(0., 0., 0.),
        glm::vec3(0., 1., 0.),
        glm::vec3(1., 0., 0.),
        1.0
    );
    auto skybox_triangles = skybox.get_triangles();

    // vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
    Plain screen(glm::vec3(0., 0., 0.), glm::vec3(0., 1.0, 0.), glm::vec3(1.0, 0., 0.));
    auto screen_triangles = screen.get_triangles();
    auto screen_triangles_texcoords = screen.get_triangles_texcoords();

    // Water itself
    Plain water_surface(
        glm::vec3(0.5, waterLevel, 0.5),
        glm::vec3(0., 0., -waterSurfaceSize),
        glm::vec3(waterSurfaceSize, 0., 0.)
    );
    auto water_surface_triangles = water_surface.get_triangles();
    auto water_surface_triangles_texcoords = water_surface.get_triangles_texcoords();

    //=========================================================

    //Создаем буфер VertexBufferObject для хранения координат на видеокарте
    GLuint vbo_vert;
    glGenBuffers(1, &vbo_vert);

    //Делаем этот буфер текущим
    glBindBuffer(GL_ARRAY_BUFFER, vbo_vert);

    //Копируем содержимое массива в буфер на видеокарте
    glBufferData(GL_ARRAY_BUFFER, 9 * triangles.size() * sizeof(float), triangles.data(), GL_STATIC_DRAW);

    //Аналогично для нормалей и меша
    GLuint vbo_norm;
    glGenBuffers(1, &vbo_norm);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_norm);
    glBufferData(GL_ARRAY_BUFFER, 9 * triangle_normals.size() * sizeof(float), triangle_normals.data(), GL_STATIC_DRAW);

    GLuint vbo_coords;
    glGenBuffers(1, &vbo_coords);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_coords);
    glBufferData(GL_ARRAY_BUFFER, 6 * triangle_plain_coords.size() * sizeof(float), triangle_plain_coords.data(), GL_STATIC_DRAW);

    //=========================================================

    //Создаем объект VertexArrayObject для хранения настроек полигональной модели
    GLuint vao;
    glGenVertexArrays(1, &vao);

    //Делаем этот объект текущим
    glBindVertexArray(vao);

    //Делаем буфер с координатами текущим
    glBindBuffer(GL_ARRAY_BUFFER, vbo_vert);

    //Включаем вершинные атрибуты
    glEnableVertexAttribArray(0); // for vertices

    // Устанавливаем настройки:
    //0й атрибут,
    //3 компоненты типа GL_FLOAT,
    //не нужно нормализовать,
    //0 - значения расположены в массиве впритык,
    //0 - сдвиг от начала
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(0)); // for vertices


    //Аналогично для буфера нормалей и меша
    glBindBuffer(GL_ARRAY_BUFFER, vbo_norm);
    glEnableVertexAttribArray(1); // for normals
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(0)); // for normals

    glBindBuffer(GL_ARRAY_BUFFER, vbo_coords);
    glEnableVertexAttribArray(2); // for mesh
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(0)); // for mesh


    //Биндим другой массив для защиты vao от изменений
    glBindVertexArray(0);


    // Same things for skybox vao and vbo
    GLuint vao_skybox, vbo_skybox;
    glGenBuffers(1, &vbo_skybox);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_skybox);
    glBufferData(GL_ARRAY_BUFFER, 9 * skybox_triangles.size() * sizeof(float), skybox_triangles.data(), GL_STATIC_DRAW);

    glGenVertexArrays(1, &vao_skybox);
    glBindVertexArray(vao_skybox);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(0));

    glBindVertexArray(0);

    //Фрейм буфер для создания воды
    GLuint fbo;
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);


    //Запись в этот буфер цветовой буфер сцены как текстуры
    GLuint tex_world_reflection_id;
    glGenTextures(1, &tex_world_reflection_id);
    glBindTexture(GL_TEXTURE_2D, tex_world_reflection_id);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, waterWindowWidth, waterWindowHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_world_reflection_id, 0);

    //Аналогично с буфером глубины
    GLuint rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, waterWindowWidth, waterWindowHeight); // use a single renderbuffer object for both a depth AND stencil buffer.
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

    // check if fbo is complete
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        std::cerr << "Framebuffer is not complete" << std::endl;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    //vao для текстурирования всего экрана
    GLuint screen_vao, screen_vbo, screen_texcoords_vbo;
    glGenVertexArrays(1, &screen_vao);
    glBindVertexArray(screen_vao);

    glGenBuffers(1, &screen_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, screen_vbo);
    glBufferData(GL_ARRAY_BUFFER, 9 * screen_triangles.size() * sizeof(float), screen_triangles.data(), GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(0));

    glGenBuffers(1, &screen_texcoords_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, screen_texcoords_vbo);
    glBufferData(GL_ARRAY_BUFFER, 6 * screen_triangles_texcoords.size() * sizeof(float), screen_triangles_texcoords.data(), GL_STATIC_DRAW);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(0));


    // Water surface vao, vbo
    GLuint water_surface_vao, water_surface_vbo, water_surface_texcoords_vbo;
    glGenVertexArrays(1, &water_surface_vao);
    glBindVertexArray(water_surface_vao);

    glGenBuffers(1, &water_surface_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, water_surface_vbo);
    glBufferData(GL_ARRAY_BUFFER, 9 * water_surface_triangles.size() * sizeof(float), water_surface_triangles.data(), GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(0));

    glGenBuffers(1, &water_surface_texcoords_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, water_surface_texcoords_vbo);
    glBufferData(GL_ARRAY_BUFFER, 6 * water_surface_triangles_texcoords.size() * sizeof(float), water_surface_triangles_texcoords.data(), GL_STATIC_DRAW);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(0));


    // HDR frame and render buffers for hdr and tone mapping
    GLuint hdr_fbo;
    glGenFramebuffers(1, &hdr_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, hdr_fbo);

    GLuint tex_hdr_color_id;
    glGenTextures(1, &tex_hdr_color_id);
    glBindTexture(GL_TEXTURE_2D, tex_hdr_color_id);
    // glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, windowWidth, windowHeight, 0, GL_RGBA, GL_FLOAT, nullptr);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_hdr_color_id, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, windowWidth, windowHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_hdr_color_id, 0);




    GLuint hdr_depth_rbo;
    glGenRenderbuffers(1, &hdr_depth_rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, hdr_depth_rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, windowWidth, windowHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, hdr_depth_rbo);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        std::cerr << "Framebuffer is not complete" << std::endl;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);


    //=========================================================
    //Работаем с текстурами:

    //Загружаем текстуру, назначая ей id (два способа)
    GLuint tex_snow_id = loadimage_to_texture("798SadykovData3/images/snow.jpeg");
    GLuint tex_sand_id = loadimage_to_texture("798SadykovData3/images/sand.jpeg");
    GLuint tex_grass_id = loadimage_to_texture("798SadykovData3/images/grass.jpeg");
    GLuint tex_normalmap_id = loadimage_to_texture("798SadykovData3/images/sand_normalmap.jpeg");
    GLuint tex_skybox_id = loadcubemapimage_to_texture("798SadykovData3/images/cloud_skybox.png");
    GLuint tex_water_id = loadimage_to_texture("798SadykovData3/images/water.jpeg");
    glBindTexture(GL_TEXTURE_2D, tex_water_id); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // Same texture load, but without SOIL in-box magic
    // GLuint tex_skybox_id = loadcubemapimage_to_texture("798SadykovData3/images/sand.jpeg");
    // auto [snow_image, snow_w, snow_h, snow_ch] = loadimage("798SadykovData3/images/sand.jpeg");
    // GLuint tex_snow_id;

    // glGenTextures(1, &tex_snow_id);
    // glActiveTexture(GL_TEXTURE0); // activate the texture unit first before binding texture
    // glBindTexture(GL_TEXTURE_2D, tex_snow_id); 
    // // set the texture wrapping parameters
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// set texture wrapping to GL_REPEAT (default wrapping method)
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // // set texture filtering parameters
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, snow_w, snow_h, 0, GL_RGB, GL_UNSIGNED_BYTE, snow_image.data());
    // glGenerateMipmap(GL_TEXTURE_2D);

    //=========================================================

    // Shaders
    Shader terrain_shader("798SadykovData3/shaders/terrain.vert", "798SadykovData3/shaders/terrain.frag");
    Shader skybox_shader("798SadykovData3/shaders/skybox.vert", "798SadykovData3/shaders/skybox.frag");
    Shader water_shader("798SadykovData3/shaders/water.vert", "798SadykovData3/shaders/water.frag");
    Shader hdr_shader("798SadykovData3/shaders/hdr.vert", "798SadykovData3/shaders/hdr.frag");

    //=========================================================

    //Добавляем uniform переменные
    glUseProgram(terrain_shader.id);
    GLint view = glGetUniformLocation(terrain_shader.id, "viewMatrix");
    GLint view_pos = glGetUniformLocation(terrain_shader.id, "viewPos");
    GLint model = glGetUniformLocation(terrain_shader.id, "modelMatrix");
    GLint projection = glGetUniformLocation(terrain_shader.id, "projectionMatrix");
    GLint light_color = glGetUniformLocation(terrain_shader.id, "lightColor");
    GLint light_dir = glGetUniformLocation(terrain_shader.id, "lightDir");
    GLint drop_level = glGetUniformLocation(terrain_shader.id, "dropHeight");

    glUseProgram(skybox_shader.id);
    GLint view_skybox = glGetUniformLocation(skybox_shader.id, "viewMatrix");
    GLint projection_skybox = glGetUniformLocation(skybox_shader.id, "projectionMatrix");

    glUseProgram(water_shader.id);
    GLint view_water = glGetUniformLocation(water_shader.id, "viewMatrix");
    GLint model_water = glGetUniformLocation(water_shader.id, "modelMatrix");
    GLint projection_water = glGetUniformLocation(water_shader.id, "projectionMatrix");

    glUseProgram(hdr_shader.id);
    GLint hdr_hdr = glGetUniformLocation(hdr_shader.id, "hdr");
    GLint hdr_exposure = glGetUniformLocation(hdr_shader.id, "exposure");

    //Сообщаем какой GL_TEXTURE# для какой текстуры
    glUseProgram(terrain_shader.id);
    glUniform1i(glGetUniformLocation(terrain_shader.id, "snowTexture"), 0);
    glUniform1i(glGetUniformLocation(terrain_shader.id, "sandTexture"), 1);
    glUniform1i(glGetUniformLocation(terrain_shader.id, "grassTexture"), 2);
    glUniform1i(glGetUniformLocation(terrain_shader.id, "normalMapTexture"), 3);
    glUseProgram(skybox_shader.id);
    glUniform1i(glGetUniformLocation(skybox_shader.id, "skyboxTexture"), 0);
    glUseProgram(water_shader.id);
    glUniform1i(glGetUniformLocation(water_shader.id, "waterTexture"), 0);
    glUniform1i(glGetUniformLocation(water_shader.id, "reflectionTexture"), 1);
    glUseProgram(hdr_shader.id);
    glUniform1i(glGetUniformLocation(hdr_shader.id, "hdrTexture"), 0);

    //Цикл рендеринга (пока окно не закрыто)
    int width, height;
    while (!glfwWindowShouldClose(window)) {
        //Используем время между генерациями фреймов
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        //Получаем размеры экрана (окна)
        glfwGetFramebufferSize(window, &width, &height);

        glBindFramebuffer(GL_FRAMEBUFFER, hdr_fbo);
        //Устанавливаем порт вывода на весь экран (окно)
        glViewport(0, 0, windowWidth, windowHeight);

        glEnable(GL_DEPTH_TEST); // disable depth test so screen-space quad isn't discarded due to depth test.

        //Очищаем порт вывода (буфер цвета и буфер глубины)
        glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Проверяем события ввода (здесь вызывается функция обратного вызова для обработки событий клавиатуры)
        glfwPollEvents();
        update_camera_reflection();
        viewMatrix = glm::lookAt(cameraPos, cameraPos + cameraArrow, cameraUp);
        projectionMatrix = glm::perspective(glm::radians(fov), (float)windowWidth / (float)windowHeight, 0.01f, 100.0f);

        //Подключаем шейдерную программу
        glUseProgram(terrain_shader.id);

        //Активируем и привязываем текстуру
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, tex_snow_id);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, tex_sand_id);
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, tex_grass_id);
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, tex_normalmap_id);

        //Отправляем значения в шейдерную программу
        glUniformMatrix4fv(view, 1, GL_FALSE, glm::value_ptr(viewMatrix));
        glUniform3fv(view_pos, 1, glm::value_ptr(cameraPos));
        glUniformMatrix4fv(model, 1, GL_FALSE, glm::value_ptr(modelMatrix));
        glUniformMatrix4fv(projection, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

        glUniform3fv(light_color, 1, glm::value_ptr(lightColor));
        glUniform3fv(light_dir, 1, glm::value_ptr(lightArrow));
        glUniform1f(drop_level, bottomLimit);

        //Подключаем VertexArrayObject с настойками полигональной модели
        glBindVertexArray(vao);

        //Рисуем полигональную модель (состоит из треугольников, сдвиг 0, количество вершин 3)
        glDrawArrays(GL_TRIANGLES, 0, 3 * triangles.size());

        //Отрисовываем аналогично для текстуры с отражением в fbo
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        glFrontFace(GL_CW);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glViewport(0, 0, waterWindowWidth, waterWindowHeight);
        glUniformMatrix4fv(view, 1, GL_FALSE, glm::value_ptr(viewReflectionMatrix));
        glUniformMatrix4fv(projection, 1, GL_FALSE, glm::value_ptr(projectionReflectionMatrix));
        glUniform1f(drop_level, waterLevel);
        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLES, 0, 3 * triangles.size());
        glBindVertexArray(0);
        glFrontFace(GL_CCW);


        //Аналогично отрисовываем skybox
        glBindFramebuffer(GL_FRAMEBUFFER, hdr_fbo);
        glViewport(0, 0, windowWidth, windowHeight);
        // Set variables
        glDepthFunc(GL_LEQUAL);  // change depth function so depth test passes when values are equal to depth buffer's content

        glUseProgram(skybox_shader.id);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, tex_skybox_id);

        glUniformMatrix4fv(view_skybox, 1, GL_FALSE, glm::value_ptr(viewMatrix));
        glUniformMatrix4fv(projection_skybox, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

        // Draw skybox
        glBindVertexArray(vao_skybox);


        glDrawArrays(GL_TRIANGLES, 0, 3 * skybox_triangles.size());

        glFrontFace(GL_CW);
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        glViewport(0, 0, waterWindowWidth, waterWindowHeight);
        glUniformMatrix4fv(view_skybox, 1, GL_FALSE, glm::value_ptr(viewReflectionMatrix));
        glUniformMatrix4fv(projection_skybox, 1, GL_FALSE, glm::value_ptr(projectionReflectionMatrix));
        glDrawArrays(GL_TRIANGLES, 0, 3 * skybox_triangles.size());
        glFrontFace(GL_CCW);

        glBindVertexArray(0);
        glDepthFunc(GL_LESS); // set depth function back to default


        // Draw subscreen and water
        glBindFramebuffer(GL_FRAMEBUFFER, hdr_fbo);
        glViewport(0, 0, windowWidth, windowHeight);
        glUseProgram(water_shader.id);
        // glDisable(GL_DEPTH_TEST); // disable depth test so screen-space quad isn't discarded due to depth test.

        // clear all relevant buffers
        // glClear(GL_COLOR_BUFFER_BIT);

        glBindVertexArray(water_surface_vao);
        glDisable(GL_CULL_FACE);

        glUniformMatrix4fv(model_water, 1, GL_FALSE, glm::value_ptr(waterModelMatrix));
        glUniformMatrix4fv(view_water, 1, GL_FALSE, glm::value_ptr(viewMatrix));
        glUniformMatrix4fv(projection_water, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, tex_water_id);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, tex_world_reflection_id);
        glDrawArrays(GL_TRIANGLES, 0, 3 * water_surface_triangles.size());

        glEnable(GL_CULL_FACE);

        //preprocess in hdr shader
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, width, height);
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f); // set clear color to white (not really necessary actually, since we won't be able to see behind the quad anyways)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        // glClear(GL_COLOR_BUFFER_BIT);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_CULL_FACE);
        glUseProgram(hdr_shader.id);

        glBindVertexArray(screen_vao);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, tex_hdr_color_id);
        glUniform1i(hdr_hdr, hdr);
        glUniform1f(hdr_exposure, exposure);

        glDrawArrays(GL_TRIANGLES, 0, 3 * screen_triangles.size());
        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);

        glBindVertexArray(0);
        glfwSwapBuffers(window); //Переключаем передний и задний буферы
    }

    SOIL_save_screenshot(
        "test_app.png",
        SOIL_SAVE_TYPE_PNG,
        0, 0, width, height
    );

    //Удаляем созданные объекты OpenGL
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo_vert);
    glDeleteBuffers(1, &vbo_norm);
    glDeleteBuffers(1, &vbo_coords);
    glDeleteVertexArrays(1, &vao_skybox);
    glDeleteBuffers(1, &vbo_skybox);
    glDeleteFramebuffers(1, &fbo);

    glfwTerminate();

    return 0;
}