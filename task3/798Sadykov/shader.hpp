#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "loaders.h"


class Shader {
 public:
  Shader(std::string vertex_shader_path, std::string fragment_shader_path);
  ~Shader();

 private:
  GLuint loadshader(std::string filename, GLuint shader_type);
  GLuint createshaderprogram();

 public:
  GLuint id;
 private:
  GLuint vertex_shader;
  GLuint fragment_shader;
};


Shader::Shader(std::string vertex_shader_path, std::string fragment_shader_path) {
  vertex_shader = loadshader(vertex_shader_path, GL_VERTEX_SHADER);
  fragment_shader = loadshader(fragment_shader_path, GL_FRAGMENT_SHADER);
  id = createshaderprogram();
}

Shader::~Shader() {
  glDeleteProgram(id);
  glDeleteShader(vertex_shader);
  glDeleteShader(fragment_shader);
}

GLuint Shader::loadshader(std::string filename, GLuint shader_type) {
  std::string shaderText = loadtext(filename);
  const GLchar* shaderTextPtr = static_cast<const GLchar*>(shaderText.c_str());

  //Создаем шейдерный объект
  GLuint shader = glCreateShader(shader_type);

  //Передаем в шейдерный объект текст шейдера
  glShaderSource(shader, 1, &shaderTextPtr, nullptr);

  //Компилируем шейдер
  glCompileShader(shader);

  //Проверяем ошибки компиляции
  int status = -1;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  if (status != GL_TRUE) {
    GLint errorLength;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &errorLength);

    std::vector<char> errorMessage;
    errorMessage.resize(errorLength);

    glGetShaderInfoLog(shader, errorLength, 0, errorMessage.data());

    std::cerr
      << "Failed to compile [" << filename
      << "] shader:\n" << errorMessage.data() << std::endl;

    exit(1);
  }

  return shader;
}

GLuint Shader::createshaderprogram() {
  //Создаем шейдерную программу
  GLuint program_id = glCreateProgram();
  glUseProgram(program_id);

  //Прикрепляем шейдерные объекты
  glAttachShader(program_id, vertex_shader);
  glAttachShader(program_id, fragment_shader);

  //Линкуем программу
  glLinkProgram(program_id);

  //Проверяем ошибки линковки
  int status = -1;
  glGetProgramiv(program_id, GL_LINK_STATUS, &status);
  if (status != GL_TRUE) {
    GLint errorLength;
    glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &errorLength);

    std::vector<char> errorMessage;
    errorMessage.resize(errorLength);

    glGetProgramInfoLog(program_id, errorLength, 0, errorMessage.data());

    std::cerr << "Failed to link the shader program:\n" << errorMessage.data() << std::endl;

    exit(1);
  }

  return program_id;
}