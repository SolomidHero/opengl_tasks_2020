#version 330

layout (location = 0) in vec3 vertexPosition;
layout (location = 1) in vec2 textureCoords;

out vec2 TexCoords;

void main() {
  TexCoords = textureCoords;
  gl_Position = vec4(vertexPosition.xyz, 1.0);
}