#version 330

uniform samplerCube skyboxTexture;

in vec3 TextureCoords;

out vec4 fragColor;

void main() {
  fragColor = vec4(texture(skyboxTexture, TextureCoords).xyz, 1.0);
}