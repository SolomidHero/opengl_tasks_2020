#version 330 core

uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat4 projectionMatrix;

layout (location = 0) in vec3 vertexPosition;
layout (location = 1) in vec2 textureCoords;

out vec2 TexCoords;
out vec2 waterTexCoords;

void main() {
  // gl_Position = vec4(vertexPosition.xyz, 1.0);
  vec4 pos = modelMatrix * vec4(vertexPosition.xyz, 1.0);
  gl_Position = projectionMatrix * viewMatrix * pos;
  TexCoords = textureCoords;
  waterTexCoords = pos.xz;
  // TexCoords = (vec2(vertexPosition.x, vertexPosition.y) + 1.0) / 2.0;
}