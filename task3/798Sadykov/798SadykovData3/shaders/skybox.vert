#version 330

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;

out vec3 TextureCoords;

void main() {
  mat4 view = mat4(mat3(viewMatrix)); // remove translation from the view matrix
  TextureCoords = vertexPosition;
  gl_Position = projectionMatrix * view * vec4(vertexPosition, 1.0);
  gl_Position = gl_Position.xyww;
}