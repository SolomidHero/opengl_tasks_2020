#version 330

uniform sampler2D hdrTexture;
uniform bool hdr;
uniform float exposure;

in vec2 TexCoords;

out vec4 fragColor;

const float gamma = 2.2;

void main() {
  vec3 hdrColor = texture(hdrTexture, TexCoords).rgb;
  vec3 result = hdrColor;
  if (hdr) {
    // reinhard
    // vec3 result = hdrColor / (hdrColor + vec3(1.0));
    // exposure
    result = vec3(1.0) - exp(-hdrColor * exposure);
  }
  // also gamma correct while we're at it
  // result = pow(result, vec3(1.0 / gamma));
  fragColor = vec4(result, 1.0);
}