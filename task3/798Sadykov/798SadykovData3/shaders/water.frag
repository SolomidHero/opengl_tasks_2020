#version 330 core

uniform sampler2D waterTexture;
uniform sampler2D reflectionTexture;

in vec2 TexCoords;
in vec2 waterTexCoords;

out vec4 FragColor;

void main() {
  float water_alpha = 0.2;
  float reflection_alpha = 0.4;
  float refraction_alpha = 1.0 - water_alpha - reflection_alpha;

  FragColor =
    texture(waterTexture, waterTexCoords) * water_alpha +
    texture(reflectionTexture, TexCoords) * reflection_alpha;
}