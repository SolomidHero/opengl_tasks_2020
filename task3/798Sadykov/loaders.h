#pragma once

#include <fstream>
#include <sstream>
#include <SOIL2.h>

std::string loadtext(const std::string& filename) {
  std::ifstream file(filename);
  std::cout << filename << std::endl;
  std::stringstream buffer;
  buffer << file.rdbuf() << "\0";

  return buffer.str();
}


std::tuple<std::vector<unsigned char>, size_t, size_t, size_t> loadimage(const std::string& filename) {
  int width, height, channels;
  unsigned char* image_data = SOIL_load_image(
    filename.c_str(),
    &width, &height, &channels,
    SOIL_LOAD_AUTO
  );
  if (image_data == 0) {
    std::cerr << "SOIL image loading [" << filename << "] error: " << SOIL_last_result() << std::endl;
    exit(1);
  } else {
    std::cout << "Image [" << filename << "] manually loaded." << std::endl;
  }

  std::vector<unsigned char> image(image_data, image_data + channels * width * height);

  SOIL_free_image_data(image_data);

  return std::make_tuple(image, width, height, channels);
}


// returns loaded texture id
GLuint loadimage_to_texture(const std::string& filename) {

  GLuint tex_2d = SOIL_load_OGL_texture(
    filename.c_str(),
    SOIL_LOAD_AUTO,
    SOIL_CREATE_NEW_ID,
    SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
  );

  if (tex_2d == 0) {
    std::cerr << "SOIL loading [" << filename << "] error: " << SOIL_last_result() << std::endl;
    exit(1);
  }

  return tex_2d;
}


std::tuple<std::array<std::vector<unsigned char>, 6>, size_t, size_t, size_t> loadcubemapimage(const std::string& filename) {
  auto [image, total_w, total_h, ch] = loadimage(filename);
  std::vector<std::pair<size_t, size_t>> block_pos = {
    {1, 2}, {1, 0}, // right, left
    {0, 1}, {2, 1}, // top, bottom
    {1, 1}, {1, 3}, // front, back
  };

  int w = total_w / 4;
  int h = total_h / 3;
  // cubemap requires square size and divisible by 4
  w = std::min(w, h);
  h = std::min(w, h);
  int pad_w = std::max(0, (static_cast<int>(total_w) - w * 4) / 2);
  int pad_h = std::max(0, (static_cast<int>(total_h) - h * 3));


  std::array<std::vector<unsigned char>, 6> faces;
  for (int i = 0; i < faces.size(); ++i) {
    auto [h_pos, w_pos] = block_pos[i];
    size_t block_start_pos = ((h_pos * h + pad_h) * total_w + w_pos * w) * ch;

    faces[i].reserve(w * h * ch);
    for (int row_i = 0; row_i < h; ++row_i) {
      faces[i].insert(
        faces[i].begin() + w * ch * row_i,
        image.begin() + block_start_pos + (row_i * total_w + pad_w) * ch,
        image.begin() + block_start_pos + (row_i * total_w + pad_w + w) * ch
      );
    }
  }

  return std::make_tuple(faces, w, h, ch);
}

// returns loaded texture id
GLuint loadcubemapimage_to_texture(const std::string& filename) {
  while (glGetError() != GL_NO_ERROR) {
    continue;
  }
  auto [faces, w, h, ch] = loadcubemapimage(filename);
  GLenum format_type = (ch == 4)? GL_RGBA : GL_RGB;
  GLuint cubemap_id;
  glGenTextures(1, &cubemap_id);
  glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap_id);

  for (size_t i = 0; i < faces.size(); ++i) {
    glTexImage2D(
      GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 
      0, format_type, w, h, 0, format_type, GL_UNSIGNED_BYTE, faces[i].data()
    );

    auto status = glGetError();
    if (status != GL_NO_ERROR) {

      std::cerr
        << "Failed creating cubemap texture [" << filename
        << "]:\n" << status << std::endl;

      exit(1);
    }
  }

  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

  return cubemap_id;
}