#include <GL/glew.h>
#include <glm/vec3.hpp>
#include <glm/gtx/normal.hpp>

#include <vector>
#include <array>


class Plain {
 public:
  Plain(glm::vec3 center, glm::vec3 up, glm::vec3 right);

  std::vector<std::array<glm::vec3, 3>> get_triangles();
  std::vector<std::array<glm::vec2, 3>> get_triangles_texcoords();
  std::vector<glm::vec3> get_triangles_normals();

 private:
  size_t to_index(int x, int y);
  std::array<int, 2> to_pos(size_t index);
  glm::vec2 pos2texcoord(std::array<int, 2> pos);

 private:
  std::array<glm::vec3, 4> vertices;
  std::array<std::array<size_t, 3>, 2> faces;
};

Plain::Plain(glm::vec3 center, glm::vec3 up, glm::vec3 right) {
  for (size_t i = 0; i < vertices.size(); ++i) {
    auto [x, y] = to_pos(i);

    vertices[i] = (
      center +
      static_cast<float>(x) * right +
      static_cast<float>(y) * up
    );
  }

  faces = {{
    {to_index(-1, 1), to_index(1, -1), to_index(-1, -1)},
    {to_index(1, -1), to_index(-1, 1), to_index(1, 1)},
  }};
}

size_t Plain::to_index(int x, int y) {
  assert(x == 1 || x == -1);
  assert(y == 1 || y == -1);

  // in [0, 3]
  return (x + 2 * y + 3) / 2;
}

std::array<int, 2> Plain::to_pos(size_t index) {
  return {
    (static_cast<int>(index) & 1)? 1 : -1,
    (static_cast<int>(index) & 2)? 1 : -1,
  };
}

glm::vec2 Plain::pos2texcoord(std::array<int, 2> pos) {
  return glm::vec2(
    (static_cast<float>(pos[0]) + 1) / 2,
    (static_cast<float>(pos[1]) + 1) / 2
  );
}


std::vector<std::array<glm::vec3, 3>> Plain::get_triangles() {
  std::vector<std::array<glm::vec3, 3>> triangles(faces.size());
  for (size_t i = 0; i < faces.size(); ++i) {
    auto [v1, v2, v3] = faces[i];
    triangles[i] = { vertices[v1], vertices[v2], vertices[v3] };
  }

  return triangles;
}

std::vector<std::array<glm::vec2, 3>> Plain::get_triangles_texcoords() {
  std::vector<std::array<glm::vec2, 3>> triangles_tex_coords(faces.size());
  for (size_t i = 0; i < faces.size(); ++i) {
    auto [v1, v2, v3] = faces[i];
    triangles_tex_coords[i] = {
      pos2texcoord(to_pos(v1)),
      pos2texcoord(to_pos(v2)),
      pos2texcoord(to_pos(v3))
    };
  }

  return triangles_tex_coords;
}


std::vector<glm::vec3> Plain::get_triangles_normals() {
  std::vector<glm::vec3> triangles_normals(faces.size());
  for (size_t i = 0; i < faces.size(); ++i) {
    auto [v1, v2, v3] = faces[i];
    triangles_normals[i] = glm::triangleNormal(vertices[v1], vertices[v2], vertices[v3]);
  }

  return triangles_normals;
}
