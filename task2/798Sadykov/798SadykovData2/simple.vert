#version 330

uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 textureCoords;

out float height;
out vec3 position;
out vec3 normal;
out vec2 texturePos;

void main() {
	height = vertexPosition.y;
	vec4 pos = modelMatrix * vec4(vertexPosition, 1.0);
	gl_Position = projectionMatrix * viewMatrix * pos;
	position = pos.xyz;

	normal = mat3(transpose(inverse(modelMatrix))) * vertexNormal;

	texturePos = vec2(textureCoords.y, textureCoords.x);
	// texturePos = textureCoords;
}