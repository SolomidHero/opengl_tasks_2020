#version 330

in float height;
in vec3 position;
in vec3 normal;
in vec2 texturePos;

uniform vec3 lightDir;
uniform vec3 lightColor;
uniform vec3 viewPos; 

uniform sampler2D snowTexture;
uniform sampler2D sandTexture;
uniform sampler2D grassTexture;
uniform sampler2D normalMapTexture;

out vec4 fragColor;

float sigmoid(float x) {
  return 1. / (1. + exp(-x));
}

float normal_density(float x, float mu) {
  return exp(-pow(x - mu, 2));
}


void main() {
	// mixing textures by their height scores
  float h = height + 0.3;
  float scale = 15;

  float snow_score = sigmoid((-1 + h) * scale);
  float grass_score = normal_density(h * scale, 0.0);
  float sand_score = sigmoid((-1 - h) * scale);
  float score_sum = snow_score + grass_score + sand_score;

  snow_score /= score_sum;
  grass_score /= score_sum;
  sand_score /= score_sum;

  vec3 objectColor =
    snow_score * texture(snowTexture, texturePos).xyz +
    grass_score * texture(grassTexture, texturePos).xyz +
    sand_score * texture(sandTexture, texturePos).xyz;

  // fragment normal
  vec3 map_norm = normalize(texture(normalMapTexture, texturePos).rbg * 2 - 1.0);
  vec3 norm = normalize(normal);
  // vec3 norm = map_norm;

  // ambient
  float ambientStrength = 0.1;
  vec3 ambient = ambientStrength * lightColor;

  // diffuse 
  float diff = max(dot(norm, lightDir), 0.1);
  vec3 diffuse = diff * lightColor;

  // specular
  float specularStrength =
    0.3 * snow_score +
    0.01 * snow_score +
    0.1 * sand_score;
  vec3 viewDir = normalize(viewPos - position);
  vec3 reflectDir = reflect(-lightDir, norm);
  float spec = pow(max(dot(viewDir, reflectDir), 0.0), 64);
  vec3 specular = specularStrength * spec * lightColor;

  vec3 result = (ambient + diffuse + specular) * objectColor;
  fragColor = vec4(result, 1.0);
}