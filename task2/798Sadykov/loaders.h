#include <fstream>
#include <sstream>
#include <SOIL2.h>

std::string loadshader(const std::string& filename) {
  std::ifstream file(filename);
  std::cout << filename << std::endl;
  std::stringstream buffer;
  buffer << file.rdbuf() << "\0";

  std::cout << buffer.str() << std::endl;
  return buffer.str();
}


std::tuple<std::vector<unsigned char>, size_t, size_t, size_t> loadimage(const std::string& filename) {
  int width, height, channels;
  unsigned char* image_data = SOIL_load_image(
    filename.c_str(),
    &width, &height, &channels,
    SOIL_LOAD_AUTO
  );

  std::vector<unsigned char> image(image_data, image_data + channels * width * height);

  SOIL_free_image_data(image_data);

  return std::make_tuple(image, width, height, channels);
}


// returns loaded texture id
GLuint loadimage_to_texture(const std::string& filename) {

  GLuint tex_2d = SOIL_load_OGL_texture(
    filename.c_str(),
    SOIL_LOAD_AUTO,
    SOIL_CREATE_NEW_ID,
    SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
  );

  if (tex_2d == 0) {
    std::cerr << "SOIL loading error: " << SOIL_last_result() << std::endl;
    exit(1);
  }

  return tex_2d;
}