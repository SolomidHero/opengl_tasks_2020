set(SRC_FILES main.cpp terrain.hpp noise.hpp loaders.h)

MAKE_OPENGL_TASK(798Sadykov 2 "${SRC_FILES}")

if (UNIX)
    target_link_libraries(798Sadykov2)
endif()
